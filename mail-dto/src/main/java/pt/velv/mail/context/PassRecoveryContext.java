package pt.velv.mail.context;

import java.io.Serializable;
import java.util.Map;

public class PassRecoveryContext extends BaseContext implements Serializable  {

    private static final long serialVersionUID = -2125266849571698241L;

    private String username;
    private String resetPasswordLink;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getResetPasswordLink() {
        return resetPasswordLink;
    }

    public void setResetPasswordLink(String resetPasswordLink) {
        this.resetPasswordLink = resetPasswordLink;
    }

    public Map<String, Object> buildContextMap(){
        return super.buildContextMap(this);
    }
}