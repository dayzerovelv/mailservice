package pt.velv.mail.context;

import com.google.common.base.CaseFormat;
import pt.velv.mail.dto.MailDto;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseContext {

    MailDto mailDto;

    Map<String, Object> buildContextMap(Object object){
        Map<String, Object> contextMap = new HashMap<>();
        Class clazz = object.getClass();
        for(Field field : clazz.getDeclaredFields()){
            if(!field.getName().contains("VersionUID")) {
                String getterName = "get" + CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, field.getName());
                try {
                    Method getter = clazz.getMethod(getterName, null);
                    contextMap.put(field.getName(), getter.invoke(object));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        return contextMap;
    }

    public MailDto getMailDto() {
        return mailDto;
    }

    public void setMailDto(MailDto mailDto) {
        this.mailDto = mailDto;
    }
}
