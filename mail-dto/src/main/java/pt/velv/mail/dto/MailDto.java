package pt.velv.mail.dto;

import java.io.Serializable;
import java.util.List;

public class MailDto implements Serializable {

    private static final long serialVersionUID = 4L;

    private String company;
    private List<String> to;
    private String subject;
    private List<AttachmentDto> attachments;
    private boolean bcc;
    private List<String> bccAddresses;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public List<String> getTo() {
        return to;
    }

    public void setTo(List<String> to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<AttachmentDto> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentDto> attachments) {
        this.attachments = attachments;
    }

    public boolean isBcc() {
        return bcc;
    }

    public void setBcc(boolean bcc) {
        this.bcc = bcc;
    }

    public List<String> getBccAddresses() {
        return bccAddresses;
    }

    public void setBccAddresses(List<String> bccAddresses) {
        this.bccAddresses = bccAddresses;
    }
}
