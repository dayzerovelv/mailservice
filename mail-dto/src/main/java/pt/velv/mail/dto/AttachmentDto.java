package pt.velv.mail.dto;

import java.io.InputStream;
import java.io.Serializable;

public class AttachmentDto implements Serializable {

    private static final long serialVersionUID = -5570672320659698632L;

    private InputStream dataStr;
    private String filename;
    private String mimeType;
    private boolean inline;

    public InputStream getDataStr() {
        return dataStr;
    }

    public void setDataStr(final InputStream dataStr) {
        this.dataStr = dataStr;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(final String filename) {
        this.filename = filename;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(final String mimeType) {
        this.mimeType = mimeType;
    }

    public boolean isInline() {
        return inline;
    }

    public void setInline(final boolean inline) {
        this.inline = inline;
    }
}
