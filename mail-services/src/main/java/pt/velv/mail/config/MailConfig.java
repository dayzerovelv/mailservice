package pt.velv.mail.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.mail.PasswordAuthentication;
import javax.mail.URLName;
import java.util.Properties;

@PropertySource("classpath:mail.properties")
public abstract class MailConfig {

    @Value("${mail.host}")
    private String host;
    @Value("${mail.port}")
    private Integer port;
    @Value("${mail.from}")
    private String from;
    @Value("${mail.username}")
    private String username;
    @Value("${mail.password}")
    private String password;
    @Value("${mail.protocol}")
    private String protocol;
    @Value("${mail.auth}")
    private Boolean auth;
    @Value("${mail.tlsEnable}")
    private Boolean tlsEnable;
    @Value("${mail.tlsRequired}")
    private Boolean tlsRequired;

    protected String getFrom(){
        return this.from;
    }

    protected JavaMailSenderImpl configureMailSender() {

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setProtocol(protocol);
        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setUsername(username);
        mailSender.setPassword(password);

        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail." + protocol + ".auth", auth);
        javaMailProperties.put("mail." + protocol + ".starttls.enable", tlsEnable);
        javaMailProperties.put("mail." + protocol + ".starttls.required", tlsRequired);

        mailSender.setJavaMailProperties(javaMailProperties);

        if (auth) {
            mailSender.getSession().setPasswordAuthentication(new URLName(host), new PasswordAuthentication(username, password));
        }

        return mailSender;
    }
}
