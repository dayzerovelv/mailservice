package pt.velv.mail.config;

public enum MailTemplatesEnum {

    PASS_RECOVERY("passRecoveryTemplate"),
    USER_REGISTER("registerTemplate")
    ;

    private String template;

    MailTemplatesEnum(final String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }
}
