package pt.velv.mail.service;

import java.io.IOException;
import java.util.Map;

import javax.mail.MessagingException;

import pt.velv.mail.dto.MailDto;

public interface EmailNotificationService {

    void sendMail(MailDto mailDto, String mailTemplate, Map<String, Object> templateVars) throws MessagingException, IOException;
}
