package pt.velv.mail.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.google.common.io.Files;

import pt.velv.mail.config.MailConfig;
import pt.velv.mail.dto.AttachmentDto;
import pt.velv.mail.dto.MailDto;

@Service
public class EmailNotificationServiceImpl extends MailConfig implements EmailNotificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailNotificationServiceImpl.class);

    @Autowired
    private TemplateEngine templateEngine;

    @Override
    public void sendMail(final MailDto mailDto, final String mailTemplate, final Map<String, Object> templateVars) throws MessagingException, IOException {
        LOGGER.info("Sending e-mail");
        final JavaMailSenderImpl mailSender = configureMailSender();
        final MimeMessage mimeMessage = mailSender.createMimeMessage();
        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
        message.setSubject(mailDto.getSubject());
        message.setFrom(getFrom());
        message.setTo(mailDto.getTo().toArray(new String[mailDto.getTo().size()]));
        
        if (mailDto.isBcc()) {
            message.setBcc(mailDto.getBccAddresses().toArray(new String[mailDto.getBccAddresses().size()]));
        }

        if (mailDto.getAttachments() != null) {
            for (final AttachmentDto attachment : mailDto.getAttachments()) {
            	final InputStream dataStr = attachment.getDataStr();
                final ByteArrayResource bar = new ByteArrayResource(IOUtils.toByteArray(dataStr));
                message.addAttachment(attachment.getFilename(), bar, attachment.getMimeType());
                if(attachment.isInline()) {
                    final String filenameWithoutExtension = Files.getNameWithoutExtension(attachment.getFilename());
                    templateVars.put(filenameWithoutExtension, attachment.getFilename());
                    message.addInline(attachment.getFilename(), bar, attachment.getMimeType());
                }
            }
        }

        final Context templateContext = new Context();
        templateContext.setVariables(templateVars);
        final String htmlContent = this.templateEngine.process(mailTemplate, templateContext);
        message.setText(htmlContent, true);

        mailSender.send(mimeMessage);
        LOGGER.info("E-mail sent successfully");
    }
}
