package pt.velv.mail.service;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pt.velv.mail.dto.AttachmentDto;
import pt.velv.mail.dto.MailDto;
import pt.velv.mail.service.EmailNotificationService;

@RunWith(SpringJUnit4ClassRunner.class)
@PropertySource("mail.properties")
@ContextConfiguration(locations = {"classpath:spring-mail-services.xml"})
public class EmailNotificationServiceUT {

	@Autowired
	private EmailNotificationService emailNotificationService;

	@Test
	public void mailSendTest() {
		final MailDto mailDto = new MailDto();

		mailDto.setBcc(false);
		mailDto.setCompany("Velv");
		mailDto.setSubject("Mail Subject");
		mailDto.setTo(Arrays.asList("bruno.crespo@dayzero.pt"));

		final String mailTemplate = "templateVelv";
		final Map<String, Object> templateVars = new HashMap<>();

		try {
			emailNotificationService.sendMail(mailDto, mailTemplate, templateVars);
		} catch (final MessagingException | IOException e) {
			Assert.fail("Exception: " + e.getMessage());
		}
	}

	@Test
	public void mailSendWithAttachmentTest() {
		final MailDto mailDto = new MailDto();

		mailDto.setBcc(false);
		mailDto.setCompany("Velv");
		mailDto.setSubject("Mail Subject");
		mailDto.setTo(Arrays.asList("bruno.crespo@dayzero.pt"));

		final Resource resource = new ClassPathResource("/templates/meme.jpg");

		final AttachmentDto attachmentDto = new AttachmentDto();
		attachmentDto.setFilename(resource.getFilename());
		attachmentDto.setInline(true);

		try {
			attachmentDto.setDataStr(resource.getInputStream());
		} catch (final IOException e) {
			Assert.fail("Exception: " + e.getMessage());
		}

		attachmentDto.setMimeType("image/jpg");

		mailDto.setAttachments(Arrays.asList(attachmentDto));

		final String mailTemplate = "templateVelv";
		final Map<String, Object> templateVars = new HashMap<>();

		try {
			emailNotificationService.sendMail(mailDto, mailTemplate, templateVars);
		} catch (final MessagingException | IOException e) {
			Assert.fail("Exception: " + e.getMessage());
		}
	}
}
